
package routines;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitUtil {

	public static void send(String [] connect, String queue_name, String queue_value) {
		try {
			send(connect[0].split(":")[0], connect[0].split(":")[1].split(",")[0], 
					connect[0].split(",")[1], connect[0].split(",")[2], queue_name, queue_value);
		} catch (Exception e) {
			try {
				send(connect[1].split(":")[0], connect[1].split(":")[1].split(",")[0], 
						connect[1].split(",")[1], connect[1].split(",")[2], queue_name, queue_value);
			} catch (Exception e2) {
				try {
					send(connect[2].split(":")[0], connect[2].split(":")[1].split(",")[0], 
							connect[2].split(",")[1], connect[2].split(",")[2], queue_name, queue_value);
				} catch (Exception e3) {
					e.printStackTrace();
				}
			}
		}
    }
	
	private static void send(String host, String port, String user, String pass, String QUEUE_NAME, String message) 
			throws IOException, TimeoutException{
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
        factory.setPort(Integer.parseInt(port));
        factory.setUsername(user);
        factory.setPassword(pass);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Sent '" + message + "'");   
	}
}
	



/*public static void getMessage(String host, int port, String user, String pass, String QUEUE_NAME) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        factory.setUsername(user);
        factory.setPassword(pass);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        
        //List<String> messages = new ArrayList<>();

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");
            //messages.add(message);
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
	
}*/