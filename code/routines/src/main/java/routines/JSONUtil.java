package routines;

import org.json.JSONObject;

public class JSONUtil {

    public static String getValue(String body, String parameter) {
        JSONObject obj = new JSONObject(body);
        return obj.getString(parameter);
    }
    
    public static String writeJSON(String[] ... a) {
    	JSONObject obj = new JSONObject();
    	for(int i=0; i<a.length; i++) {
    		obj.put((a[i])[0], (a[i])[1]);
    	}
    	return obj.toString();
    }
}
